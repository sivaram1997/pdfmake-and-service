import { Component, OnInit } from '@angular/core';
import { MyServiceService } from '../my-service.service';
import * as  jsPDF  from 'jspdf';
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;
@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements OnInit {

  formValue:any;
  myString:any;
  constructor(
    private myServiceService : MyServiceService
  ) { }

  ngOnInit() {
    this.formValue = this.myServiceService.getMyFormValue();
    console.log(this.formValue)
    this.myString=JSON.stringify(this.formValue)

  }
  docDefinition = { content: '' };
download(){
  // this.docDefinition.content = this.formValue;
    this.docDefinition.content=this.myString;
    pdfMake.createPdf(this.docDefinition).download();


}
}
