import { Component, OnInit } from '@angular/core';
import { MyServiceService } from '../my-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router'
@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {
  mark=60;
  MyFrom: FormGroup;
  constructor(
    private form: FormBuilder, 
    private myServiceService: MyServiceService,
    private  router: Router,
    ) {
    this.MyFrom = this.form.group({
      name: ['', Validators.required],
      address: ['', Validators.required],
    })
  }



  ngOnInit() {

  }

  onSubmit() {
    console.log(this.MyFrom.value)
    this.myServiceService.setMyFormValue(this.MyFrom.value);
    this.router.navigate(['/list']);
  }

}