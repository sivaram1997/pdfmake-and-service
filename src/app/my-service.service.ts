import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MyServiceService {

  myFormValue: any;
  constructor() { }

  setMyFormValue(value) {
    this.myFormValue = value;
  }

  getMyFormValue() {
    return this.myFormValue;
  }
}
